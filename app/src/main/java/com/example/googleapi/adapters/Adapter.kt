package com.example.googleapi.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.googleapi.Models.MyModel
import com.example.googleapi.R
import kotlinx.android.synthetic.main.items_view.view.*

class Adapter(val itemslist:MutableList<MyModel>): RecyclerView.Adapter<Adapter.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.items_view,parent,false))
     }

    override fun getItemCount()=itemslist.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onbind()
    }
    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        lateinit var model: MyModel
        fun onbind(){
            model=itemslist[adapterPosition]
            itemView.description.text=model.description
        }
    }
}