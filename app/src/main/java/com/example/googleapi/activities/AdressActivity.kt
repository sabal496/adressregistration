package com.example.googleapi.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import com.example.googleapi.Models.CountryesModel
import com.example.googleapi.R
import com.example.googleapi.dataloaders.ApiRequest2
import com.example.googleapi.interfaces.CallbackApi
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_adress.*
import kotlinx.android.synthetic.main.custom_chooser.view.*
import kotlinx.android.synthetic.main.dialog_vwindow.*
import kotlinx.android.synthetic.main.main_toolbar.*
import org.json.JSONObject

class AdressActivity : AppCompatActivity() {
    var mylist= mutableListOf<CountryesModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adress)
        init()
    }
    private fun init(){
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.navigationIcon!!.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        done.text="Finalize"

        street.tiitlechoser.text="Street Adress"
        street.setOnClickListener(){
            val intet= Intent(this,
                ChooseAdressActivity::class.java)
            startActivity(intet)
        }

        ApiRequest2.getRequest(ApiRequest2.METHOD,object :CallbackApi{
            override fun onResponse(value: String?) {
                    var json=JSONObject(value)
                    var jsonarray=  json.getJSONArray("results")
                    mylist=  Gson().fromJson(jsonarray.toString(),Array<CountryesModel>::class.java).toMutableList()
                var model=mylist[(0 until mylist.size).random()]
                country.chooser.text=model.name
             }

            override fun onFailure(value: String?) {

             }
        },this)

        nextbtn.setOnClickListener(){
            dialog()
        }
    }
    private fun dialog(){
        val dialog=Dialog(this)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_vwindow)

        val params:ViewGroup.LayoutParams=dialog.window!!.attributes
        params.width=ViewGroup.LayoutParams.MATCH_PARENT
        params.height=ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes=params as WindowManager.LayoutParams
        dialog.dialogbtn.setOnClickListener(){
            dialog.dismiss()
        }
        dialog.show()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId==android.R.id.home){
            super.onBackPressed()
        }
        return true
    }

}
